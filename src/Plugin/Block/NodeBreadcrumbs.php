<?php

namespace Drupal\custom_node_breadcrumbs\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides a 'Custom Node Breadcrumbs' block.
 *
 * @Block(
 *  id = "custom_node_breadcrumb_block",
 *  admin_label = @Translation("Custom Node Breadcrumb Block"),
 * )
 */
class NodeBreadcrumbs extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $route;

  /**
   * Constructs a new object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->route = $route;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $data = [];
    $homeUrl = '';
    if ($node = $this->route->getParameter('node')) {
      $homeUrl = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
      $node = $this->route->getParameter('node');
      if ($node && is_object($node)) {
        if ($node->hasField('field_breadcrumbs')) {
          $urls = $node->get('field_breadcrumbs')->getValue();
          $i = 1;
          foreach ($urls as $url) {
            $data[$i]['url_title'] = $url['title'];
            $data[$i]['url_uri'] = Url::fromUri($url['uri'])->toString();
            $i++;
          }
        }
      }
    }
    return [
      '#theme' => 'custom_node_breadcrumbs',
      '#data' => $data,
      '#home_url' => $homeUrl,
      '#cache' => ['max-age' => 0],
    ];
  }

}
