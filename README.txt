CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Custom Node Breadcrumbs module is used to create node epecific breadcrumbs.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/custom_node_breadcrumbs

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/custom_node_breadcrumbs


REQUIREMENTS
------------

    1. Create new field field_breadcrumbs of type link which allows multiple links.


INSTALLATION
------------

 * Install the [Custom Node Breadcrumbs as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Create new field field_breadcrumbs of type link which allows multiple links.
    3. Place the Custom Node Breadcrumb Block.


MAINTAINERS
-----------

 * Abhinesh Dhiman (abhinesh) - https://www.drupal.org/u/abhinesh

Supporting organizations:
 * TO THE NEW - https://www.drupal.org/to-the-new
